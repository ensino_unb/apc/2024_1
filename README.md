# Plano da Disciplina - Algoritmos e Programação de Computadores (113476)

## Professores
* Fabricio Ataides Braz
* Nilton Correia da Silva

## Período
1º Semestre de 2.024

## Turmas
* 09 - Fabricio Ataides Braz
* 10 - Nilton Correia da Silva

## Ementa
Princípios fundamentais de construção de programas. Construção de algoritmos e sua representação em pseudocódigo e linguagens de alto nível. Noções de abstração. Especificação de variáveis e funções. Testes e depuração. Padrões de soluções em programação. Noções de programação estruturada. Identificadores e tipos. Operadores e expressões. Estruturas de controle: condicional e repetição. Entrada e saída de dados. Estruturas de dados estáticas: agregados homogêneos e heterogêneos. Iteração e recursão. Noções de análise de custo e complexidade. Desenvolvimento sistemático e implementação de programas. Estruturação, depuração, testes e documentação de programas. Resolução de problemas. Aplicações em casos reais e questões ambientais.

## Método

Independente do método de ensino, a programação de computador é uma habilidade, cuja apreensão exige experimentos contínuos de exercício dos fundamentos da programação. Várias abordagens servem ao própósito de motivar o aluno a buscar esse conhecimento. 

Nosso foco é a necessidade de estimular a turma a adquirir habilidades fundamentais para o desenvolvimento de algoritmos. Em razão disso, decidimos lançar mão da instrução no início do período letivo. Momento em que passaremos pela sensibilização sobre lógica de programação, usando para isso os recursos de [programação em blocos](https://code.org), e depois fundamentos de [programação em Python](https://github.com/PenseAllen/PensePython2e/).


## Ferramentas & Materiais
* [Aprender3](https://aprender3.unb.br/course/view.php?id=22677) - Comunicação e trabalho colaborativo
* [Python](https://www.python.org/) - Linguagem de programação
* [Code](https://studio.code.org/join/NNKGJV) - Programação em blocos

|AVISO CODE|
|-|
|**Ao se inscreverem no code informem em ``nome do usuário`` o seu email do aprender.unb.br**. Isso é para podermos consolidar as notas.|

## Avaliação

Para que o aluno seja aprovado na disciplina ele deve possuir uma média final (MF) superior ou igual a 50, correspondente a menção MM. Além disso, seu comparecimento deve ser superior ou igual a 75% dos eventos estabelecidos pela disciplina. 

### Composição da Média Final (MF)

As atividades avaliativas individuais que compõem a MF do aluno são: 

* PE1, PE2, PE3: Provas Escritas.  

* ExCODE: nota correspondente ao percentual de exercícios resolvidos na plataforma code.org

* ExAprender: nota correspondente ao percentual de exercícios resolvidos no próprio Aprender

![equation](https://latex.codecogs.com/svg.image?NSeq&space;=&space;0,1*ExCODE&space;&plus;&space;0,2*ExAprender&plus;&space;0,7*PE1)

![equation](https://latex.codecogs.com/svg.image?NDec&space;=&space;0,2*ExAprender&plus;0,8*PE2)

![equation](https://latex.codecogs.com/svg.image?NRep&space;=&space;0,1*ExAprender&plus;0,9*PE3)

![equation](https://latex.codecogs.com/svg.image?MediaFinal&space;=&space;\frac{NSeq&space;&plus;2*NDec&space;&plus;&space;3*NRep}{6})



### Cronograma
|    Aulas | nova_data   | novo_dw   | Feriado          | Conteudo  |
|---------:|:------------|:----------|:-----------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1        | 18/03/24    | seg | - | Acolhida  |
| 2        | 19/03/24    | ter | - | Code.org  |
| 3        | 21/03/24    | qui | - | Code.org  |
| 4        | 25/03/24    | seg | - | Introdução a sistema|
| 5        | 26/03/24    | ter | - | Code.org  |
| 6        | 28/03/24    | qui | - |  Code.org        |
| 7        | 01/04/24    | seg | - | [Introdução a programação](https://github.com/PenseAllen/PensePython2e/blob/master/docs/01-jornada.md)|
| 8        | 02/04/24    | ter | - |   Prática de Programação     |
| 9        | 04/04/24    | qui | - |  Prática de Programação             |
| 10       | 08/04/24    | seg | - | [Variáveis e expressões](https://github.com/PenseAllen/PensePython2e/blob/master/docs/02-vars-expr-instr.md)                     |
| 11       | 09/04/24    | ter | - | Prática de Programação  |
| 12       | 11/04/24    | qui | - |  Prática de Programação |
| 13       | 15/04/24    | seg | - | [Funções](https://github.com/PenseAllen/PensePython2e/blob/master/docs/03-funcoes.md)<br/>[Funções com Resultado](https://github.com/PenseAllen/PensePython2e/blob/master/docs/06-funcoes-result.md)  |
| 14       | 16/04/24    | ter | - | Prática de Programação  |
| 15       | 18/04/24    | qui | - | Prática de Programação                                        |
| 16       | 22/04/24    | seg | - | Prova Escrita 1 (PE1) - Estruturas Sequenciais                                                               |
|  17       | 23/04/24    | ter | - | [Estrutura de Decisão](https://github.com/PenseAllen/PensePython2e/blob/master/docs/05-cond-recur.md)            |
|  18       | 25/04/24    | qui | - | Prática de Programação  |
|  19       | 29/04/24    | seg | - | [Estrutura de Decisão Aninhada](https://github.com/PenseAllen/PensePython2e/blob/master/docs/05-cond-recur.md)  |
|  20       | 30/04/24    | ter | - | Prática de Programação  |
|  21       | 02/05/24    | qui | - | Prática de Programação|
|  22       | 06/05/24    | seg | - | Prova Escrita 2 (PE2) - Estruturas de Decisão   |
|  23       | 07/05/24    | ter | - | Prática de Programação                                         |
|  24       | 09/05/24    | qui | - | Prática de Programação  |
|  25       | 13/05/24    | seg | - | [Estrutura de Repetição](https://github.com/PenseAllen/PensePython2e/blob/master/docs/07-iteracao.md)            |
|  26       | 14/05/24    | ter | - | Prática de Programação  |
|  27       | 16/05/24    | qui | - | Prática de Programação  |
|  28       | 20/05/24    | seg | - | [Listas](https://github.com/PenseAllen/PensePython2e/blob/master/docs/10-listas.md)    |
|  29       | 21/05/24    | ter | - | Prática de Programação  |
|  30       | 23/05/24    | qui | - | Prática de Programação  |
|  31       | 27/05/24    | seg | - | [Estrutura de Repetição](https://github.com/PenseAllen/PensePython2e/blob/master/docs/07-iteracao.md)            |
|  32       | 28/05/24    | ter | - | Prática de Programação  |
|  -       | 30/05/24    | qui | Corpus Christi | Feriado                                      |
|  33       | 03/06/24    | seg | - | [Strings](https://github.com/PenseAllen/PensePython2e/blob/master/docs/08-strings.md)  |
|  34       | 04/06/24    | ter | - | Prática de Programação  |
|  35       | 06/06/24    | qui | - | Prática de Programação  |
|  -       | 10/06/24    | seg | - | Sem aula - Professor em Conferência   |
|  -       | 11/06/24    | ter | - | Sem aula - Professor em Conferência    |
|  -       | 13/06/24    | qui | - | Sem aula - Professor em Conferência |
|  36       | 17/06/24    | seg | - | Estruturas de Repetições Aninhadas |
|  37       | 18/06/24    | ter | - | Estruturas de Repetições Aninhadas|
|  38       | 20/06/24    | qui | - | Prática de Programação|
|   39     | 24/06/24    | seg | - |  Prática de Programação |
|  40       | 25/06/24    | ter | - | Estruturas de Repetições - Exemplos de Aplicações |                                                                      
|  41       | 27/06/24    | qui | - | Prática de Programação  |
|  42       | 01/07/24    | seg | - | Prova Escrita 3 (PE3) |
|  43       | 02/07/24    | ter | - | Prática de Programação |
|  44       | 04/07/24    | qui | - | Prática de Programação - Estruturas de Repetições |
|  45       | 08/07/24    | seg | - | Revisão de Notas |
## Referências Bibliográficas

* Básica 
    * [Pense Python](https://github.com/PenseAllen/PensePython2e/tree/master/docs)
    * Cormen, T. et al., Algoritmos: Teoria e Prática. 3a ed., Elsevier - Campus, Rio de Janeiro, 2012 
    * Ziviani, N., Projeto de Algoritmos com implementação em Pascal e C, 3a ed., Cengage Learning, 2010. 
Felleisen, M. et al., How to design programs: an introduction to computing and programming, MIT Press, EUA, 2001. 

* Complementar 
    * Evans, D., Introduction to Computing: explorations in Language, Logic, and Machi nes, CreatSpace, 2011. 
    * Harel, D., Algorithmics: the spirit of computing, Addison-Wesley, 1978. 
    * Manber, U., Introduction to algorithms: a creative approach, Addison-Wesley, 1989. 
    * Kernighan, Brian W; Ritchie, Dennis M.,. C, a linguagem de programacao: Padrao ansi. Rio de janeiro: Campus 
    * Farrer, Harry. Programação estruturada de computadores: algoritmos estruturados. Rio de Janeiro: Guanabara Dois, 2002.
    * [Plotly](https://dash-gallery.plotly.host/Portal)
    * [Portal Brasileiro de Dados Abertos](http://dados.gov.br)
    * [Kaggle](http://kaggle.com/)
    * [Ambiente de Desenvolvimento Python](https://realpython.com/installing-python/)
    * [Python Basics](https://www.learnpython.org)
    * [the Python Code Example Handbook](https://www.freecodecamp.org/news/the-python-code-example-handbook)

* Cursos de python
    - Os alunos podem escolher qualquer fonte extra de aprendendizagem para a linguagem Python. Os recursos são enormes. Algumas sugestões iniciais são:
    * https://www.pycursos.com/python-para-zumbis/
    * https://www.youtube.com/playlist?list=PLcoJJSvnDgcKpOi_UeneTNTIVOigRQwcn
    * https://www.youtube.com/playlist?list=PLlrxD0HtieHhS8VzuMCfQD4uJ9yne1mE6
    * https://www.youtube.com/watch?v=O2xKiMl-d7Y&list=PL70CUfm2J_8SXFHovpVUbq8lB2JSuUXgk
    * https://solyd.com.br/treinamentos/python-basico/
    * https://wiki.python.org/moin/BeginnersGuide/NonProgrammers

